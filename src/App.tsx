import { PublicRouters } from "./routing";
import "./App.css";

export function App() {
  return <PublicRouters />;
}
