import { RouterMatch } from "./types";
import { useMatch as useMatchBase } from "react-router";

export const useMatch = (pattern: RouterMatch) => {
  return useMatchBase(`:` + pattern)?.params[pattern];
};

export const useTeamId = () => {
  return useMatch(RouterMatch.TeamId) || "";
};
