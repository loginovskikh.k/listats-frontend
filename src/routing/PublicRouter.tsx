import { Route, Routes } from "react-router";
import { BrowserRouter } from "react-router-dom";
import { RouterMatch } from "./types";

import { MainPage, TeamIdPage } from "../pages";

export const PublicRouters = () => (
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<MainPage />} />

      <Route path={`/:${RouterMatch.TeamId}`} element={<TeamIdPage />} />
    </Routes>
  </BrowserRouter>
);
