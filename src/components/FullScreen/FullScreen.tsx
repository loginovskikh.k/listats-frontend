import styled from "styled-components";
import { fullScreen } from "../css";

export const FullScreen = styled.span`
  ${fullScreen}
`;
