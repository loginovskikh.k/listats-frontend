import { Button as _Button } from "antd";
import { fullScreen, centered } from "../../components";
import styled from "styled-components";

export const ButtonsContainer = styled.span`
  flex-direction: column;
  gap: 20px;

  ${fullScreen}
  ${centered}
`;

export const Button = styled(_Button)`
  width: 200px;
`;
