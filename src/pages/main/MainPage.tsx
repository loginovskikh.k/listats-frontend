import { FC } from "react";
import { Link } from "react-router-dom";
import { TEAM_NAMES } from "../../constants";

import { ButtonsContainer, Button } from "./styled";

export const MainPage: FC = () => {
  return (
    <ButtonsContainer>
      <h2>Выберите вашу команду</h2>
      {Object.keys(TEAM_NAMES).map((teamName) => (
        <Link to={teamName} key={teamName}>
          <Button type="primary">{TEAM_NAMES[teamName]}</Button>
        </Link>
      ))}
    </ButtonsContainer>
  );
};
