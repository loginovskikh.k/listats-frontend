import { Link } from "react-router-dom";
import { PageHeader } from "antd";
import { Card, FullScreen } from "../../components";

import { useTeamId } from "../../routing";
import { TEAM_NAMES } from "../../constants";

export const TeamIdPage = () => {
  const teamId = useTeamId();

  return (
    <FullScreen>
      <PageHeader
        title={TEAM_NAMES[teamId]}
        subTitle={<Link to="/">Сменить команду</Link>}
      />

      <Card
        title="Турнир Личесс мега клаб"
        extra="1 место / нарисовать медаль"
        hoverable
      >
        Данные о турнире (рейтинг, перфоманс)
        <br />
        Если выбрано имя пользователя, через слеш показываем личнные данные
      </Card>
    </FullScreen>
  );
};
